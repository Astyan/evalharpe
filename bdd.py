#!/usr/bin/env python
# encoding: utf-8
"""File to get all sequence from a peptide inside the 
harpe-website bdd"""
import MySQLdb as mdb
import MySQLdb.cursors


class BddHarpe(object):
    """Class to acces and read peptide analysis of Harpe"""
    def __init__(self, name):
        """Init the connection"""
        self.con = mdb.connect("localhost",
            "root", "toor", "Harpe-website",
             cursorclass=MySQLdb.cursors.DictCursor)
        self.analyse_name = name
        self.id_analyse = []
        self.peptid_list = []
        self.seq_list = []
             
    def get_id_analyse(self):
        """Method to get the id of the analysis with the name"""
        cur = self.con.cursor()
        baserequest = "SELECT id FROM `website_analysemgf` WHERE name=%s"
        cur.execute(baserequest, (self.analyse_name))
        res = cur.fetchone()
        self.id_analyse = res['id']
    
    def get_id_peptide(self):
        """Method to get all peptids id and mz of these peptids""" 
        cur = self.con.cursor()
        lres = []
        baserequest = "SELECT id, name FROM `website_analysepeptide` \
        WHERE analyse_id=%s"
        cur.execute(baserequest, (self.id_analyse))
        res = cur.fetchall()
        for i in range(len(res)):
            mz = (res[i]['name']).split("(")[1].split(")")[0]
            lres.append((res[i]['id'], mz))
        self.peptid_list = lres
    
    def get_seq_mz(self):
        """Method to associate every sequences find to a mz"""
        cur = self.con.cursor()
        lres = []
        baserequest = "SELECT sequence_aa, score FROM \
        `website_calculatedpeptide` WHERE analyse_id=%s"
        for (analyse_id, mz) in self.peptid_list:
            cur.execute(baserequest, (analyse_id))
            res = cur.fetchall()
            for i in range(len(res)):
                lres.append((mz, (res[i]['sequence_aa'])[::-1], 
                res[i]['score']))
        self.seq_list = lres
    
    def get_sequences_from_mz(self, mzin):
        """Method to get all sequences from a mz"""
        list_seq = []
        for (mz, seq, score) in self.seq_list:
            if mz == mzin:
                list_seq.append(seq)
        return list_seq
    
    def get_sequences_score_from_mz(self, mzin):
        """ Method to get all sequences and score from a mz """
        list_seq_score = []
        for (mz, seq, score) in self.seq_list:
            if mz == mzin:
                list_seq_score.append((seq, score))
        return list_seq_score
    
    def get_all_seq_mz(self):
        """Method to do get direct all sequences and mz associate """
        self.get_id_analyse()
        self.get_id_peptide()
        self.get_seq_mz()
    
    def __del__(self):
        self.con.close()


if __name__ == "__main__":
    GETINFO = BddHarpe("bsanoelle4")
    print GETINFO.get_all_seq_mz()
    print GETINFO.get_sequences_from_mz('862.9161')
