#!/usr/bin/env python
# encoding: utf-8
"""Tests for parse.py with unittest"""

import unittest
import numpy.testing as nt
import os

import parse

class TestParser(unittest.TestCase):
    """class to test the parser"""
    
    def setUp(self):
        """To do before each tests"""
        self.mparse = parse.MascotParser(os.path.join(".", "filestest",
        "mascot.txt"))
        self.hparse = parse.HarpeParser(os.path.join(".", "filestest",
        "harpe.txt"))
        self.hseqparse = parse.MascotProtSeqParser(os.path.join
        (".", "filestest", "mascotprot.txt"))
    
    def tearDown(self):
        """Clear after each tests"""
        pass
    
    def test_00_parse_file(self):
        """ test the parsing of the three files """
        self.mparse.parse_file()
        nt.assert_string_equal(self.mparse.result_parse[0][0], "35")
        self.hparse.parse_file()
        nt.assert_string_equal(self.hparse.result_parse[0][0], 
        "Cmpd142.000000,+MSn(862.9161),36.700000min")
        self.hseqparse.parse_file()
        nt.assert_string_equal(self.hseqparse.result_parse[0][0],
        'MKWVTFISLL')
        
    def test_01_get_all_mz(self):
        """check if it still get all mz"""
        self.mparse.parse_file()
        self.hparse.parse_file()
        mz_mparse = self.mparse.get_all_mz()
        mz_hparse = self.hparse.get_all_mz()
        nt.assert_equal(len(mz_mparse), 48)
        nt.assert_equal(len(mz_hparse), 159)
    
    def test_01_get_mz(self):
        """ check if the mz value is the good value """
        self.mparse.parse_file()
        self.hparse.parse_file()
        mz_mparse = self.mparse.get_mz(0)
        mz_hparse = self.hparse.get_mz(0)
        nt.assert_string_equal(mz_mparse, "625.3100")
        nt.assert_string_equal(mz_hparse, "862.9161")
    
    def test_01_get_sequence(self):
        """check if we get the right sequence"""
        self.mparse.parse_file()
        self.hparse.parse_file()
        seq_mparse = self.mparse.get_sequence(0)
        seq_hparse = self.hparse.get_sequence(0)
        nt.assert_string_equal(seq_mparse, "FKDLGEEHFK")
        nt.assert_string_equal(seq_hparse, "MPCTEDYISII")
    
    def test_02_get_sequences_from_mz(self):
        """check if we get the right sequence with an mz"""
        self.mparse.parse_file()
        self.hparse.parse_file()
        seq_mparse = self.mparse.get_sequences_from_mz("625.3100")
        seq_hparse = self.hparse.get_sequences_from_mz("862.9161")
        self.assertIn("FKDLGEEHFK", seq_mparse)
        self.assertIn("MPCTEDYISII", seq_hparse)
    
    def test_O2_get_score_from_mz(self):
        """check if we get the right score with an mz """
        self.mparse.parse_file()
        self.hparse.parse_file()
        score_mparse = self.mparse.get_score_from_mz("625.3100")
        score_hparse = self.hparse.get_score_from_mz("862.9161")
        nt.assert_string_equal(score_hparse[0], '20.99657632654604')
        nt.assert_string_equal(score_mparse[0], '37')
    
    def test_01_get_sequence_prot(self):
        """test to get the sequence of the protein"""
        self.hseqparse.parse_file()
        res = self.hseqparse.get_sequence()
        nt.assert_string_equal(res[10:42], 
        'LLFSSAYSRGVFRRDTHKSEIAHRFKDLGEEH')
        

if __name__ == '__main__':
    unittest.main()

    
    
