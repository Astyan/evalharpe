#!/usr/bin/env python
# encoding: utf-8
"""Tests for evaluate.py with unittest"""

import unittest
import numpy.testing as nt
import os

import evaluate

class TestEvaluator(unittest.TestCase):
    """Class to test evaluate"""
    
    def setUp(self):
        """To do before each tests"""
        self.hevaluator = evaluate.HarpeEvaluator(os.path.join(".", 
        "filestest", "harpe.txt"), os.path.join(".", 
        "filestest", "mascot.txt"), os.path.join(".", 
        "filestest", "mascotprot.txt"))
        
    def tearDown(self):
        """Clear after each tests"""
        pass
    
    def test_00_best_tuple(self):
        """Test if best_tuple works"""
        t1 = (2, 0, 0)
        t2 = (1, 0, 2)
        tr = evaluate.best_tuple(t1, t2)
        nt.assert_equal(tr, t2)
        
        t1 = (2, 0, 2)
        t2 = (1, 1, 2)
        tr = evaluate.best_tuple(t1, t2)
        nt.assert_equal(tr, t1)
        
        t1 = (2, 0, 2)
        t2 = (1, 0, 2)
        tr = evaluate.best_tuple(t1, t2)
        nt.assert_equal(tr, t2)
    
    def test_00_best_match(self):
        """Test if best_match works """
        res = evaluate.best_match("AIIDCEA", "EAILCCIL")
        nt.assert_equal(res, (8, 7, 3))
    
    def test_00_change_leucine(self):
        """Test if change_leucine works"""
        res = self.hevaluator.change_leucine("AIL")
        if self.hevaluator.l_is_i:
            nt.assert_equal(["A", "I", "I"], res)
        else:
            nt.assert_string_equal(["A", "I", "L"], res)
    
    def test_00_eval_all_top1(self):
        """test if eval_all works"""
        res = self.hevaluator.eval_all_top1()
        nt.assert_equal(res[0], 656)
        nt.assert_equal(res[1], 270)
        nt.assert_equal(res[2], 224)
        nt.assert_equal(res[3], 20)
        nt.assert_equal(res[4], 18)
        

if __name__ == '__main__':
    unittest.main()
