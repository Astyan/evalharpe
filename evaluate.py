#!/usr/bin/env python
# encoding: utf-8
"""evaluate.py contain the classe to evaluate Harpe results with Mascot 
results """

from __future__ import print_function
#~ import os
import sys

from parse import MascotParser, HarpeParser, MascotProtSeqParser
from bdd import BddHarpe

def best_tuple(tuple1, tuple2):
    """ best_tuple return the best tuple works only for tuples 
    returned by best_match"""
    if tuple1[2] > tuple2[2]:
        return tuple1
    elif tuple1[2] == tuple2[2]:
        if tuple1[1] < tuple2[1]:
            return tuple1
        if tuple1[1] == tuple2[1]:
            if tuple1[0] <= tuple2[0]:
                return tuple1
    return tuple2
    
def best_match(seqharpe, seqmascot):
    """return the tuple representing the best match between 2 
    sequences.
    The mascot sequence must be at least at the size of the harpe 
    sequence"""
    size_sharpe = len(seqharpe)
    size_smascot = len(seqmascot)
    maxrec = 0
    seqmascot = seqmascot + " " * size_sharpe 
    for i in range(0, size_smascot):
        nbrec = 0
        for couple in zip(seqharpe, seqmascot[i:i + size_sharpe]):
            if couple[0] == couple[1]:
                nbrec += 1
        if maxrec < nbrec:
            maxrec = nbrec
    return (size_smascot, size_sharpe, maxrec)

def best_match_error(seqharpe, seqmascot):
    """return the bestmatch sequence with error highlighted"""
    size_sharpe = len(seqharpe)
    size_smascot = len(seqmascot)
    maxrec = 0
    seqmascotmatch = ""
    seqmascot = seqmascot + " " * size_sharpe 
    for i in range(0, size_smascot):
        nbrec = 0
        for couple in zip(seqharpe, seqmascot[i:i + size_sharpe]):
            if couple[0] == couple[1]:
                nbrec += 1
        if maxrec < nbrec:
            maxrec = nbrec
            seqmascotmatch = seqmascot[i:i + size_sharpe]
    seq_res = ""
    for i in range(len(seqharpe)):
        if seqharpe[i] == seqmascotmatch[i]:
            seq_res = seq_res + seqharpe[i]
        else:
            seq_res = seq_res + "[" + seqharpe[i] + "->" +\
            seqmascotmatch[i] + "]"
    return seq_res

def print_all_seq_list(seq_list):
    print("""mz, intensity, sequence mascot,"""+
    """ score mascot, sequence harpe, score harpe""")
    for dinfo in seq_list:
        print(dinfo["mz"]+", "+dinfo["intensity"]+", "+
        dinfo["seq_mascot"]+", "+dinfo["score_mascot"]+", "+
        dinfo["seq_harpe"]+", "+str(dinfo["score_harpe"]))

def print_stat(info):
    """ print all the static informations """
    print("%d amino acid have been reconized by Mascot"%(info[0]))
    print("%d amino acid have been reconized by Harpe"%(info[1]))
    print("%d amino acid have been correctly reconnized by Harpe"%\
    (info[2]))
    print("Harpe has reconized correctly %f percent of Mascot result"\
    %((float(info[2])/float(info[0]))*100.))
    print("Harpe has %f percent of false positive"\
    %((1.-(float(info[2])/float(info[1])))*100.))
    print("%d sequences have been well reconized"%(info[3]))
    print("%d sequences have been reconized with errors"%(info[4]))
    print("%d sequences have been reconized by mascot"%(info[5]))

class HarpeEvaluator(object):
    """This class evalue Harpe results with Mascot results"""
    
    def __init__(self, harperesults, mascotresults, mascotprot ,same_il=True, max_error=3):
        """ use the path to result and sameIL (Isoleucine and Leucine 
        considered like one AA) and create the parsers"""
        self.harperes = HarpeParser(harperesults)
        self.harperes.parse_file()
        self.mascotres = MascotParser(mascotresults)
        self.mascotres.parse_file()
        self.mascotprotres = MascotProtSeqParser(mascotprot)
        self.mascotprotres.parse_file()
        self.l_is_i = same_il
        self.error_tol = max_error
    
    
    def change_leucine(self, sequences):
        """ change all leucine of a sequence in isoleucine if 
        self.l_is_i is at True""" 
        newseq = []
        if self.l_is_i:
            for sequence in sequences:
                newseq.append(sequence.replace('L', 'I'))
            return newseq
        else:    
            return sequences
        
    def eval_all_top1(self):
        """ eval All the sequences find by harpe with mascot sequences.
        It must be the more optimistic evaluation and return a tuple 
        with the number of AA reconized by mascot, the number of AA 
        reconized by harpe and the number of AA correctly reconized by 
        harpe"""
        listofmz = self.mascotres.get_all_mz()
        nb_seqmascot = len(listofmz)
        reconized_mascot = 0
        reconized_harpe = 0
        good_reconized_harpe = 0
        seq_without_mistakes = 0
        seq_with_mistakes = 0
        for mz in listofmz:
            seqs_mascot = self.change_leucine(\
            self.mascotres.get_sequences_from_mz(mz))
            seqs_harpe = self.harperes.get_sequences_from_mz(mz)
            bestt = (0, 0, -1)
            for seq_m in seqs_mascot:
                bestt = (len(seq_m), 0, -1)
                for seq_h in seqs_harpe:
                    bestt = best_tuple(bestt, best_match(seq_h, seq_m))
            
            reconized_mascot += bestt[0]
            if bestt[2] >= self.error_tol: #if harpe has not reconized 
                #at least error_tol aas don't take the result
                #~ print(bestt)
                reconized_harpe += bestt[1]
                good_reconized_harpe += bestt[2]
                if bestt[1] == bestt[2]:
                    seq_without_mistakes += 1
                else:
                    seq_with_mistakes += 1
                    
        return (reconized_mascot, reconized_harpe, good_reconized_harpe,
        seq_without_mistakes, seq_with_mistakes, nb_seqmascot)
    
    def eval_all(self, bdd):
        """Method to eval all peptides from Harpe database"""
        listofmz = self.mascotres.get_all_mz()
        nb_seqmascot = len(listofmz)
        reconized_mascot = 0
        reconized_harpe = 0
        good_reconized_harpe = 0
        seq_without_mistakes = 0
        seq_with_mistakes = 0
        for mz in listofmz:
            seqs_mascot = self.change_leucine(\
            self.mascotres.get_sequences_from_mz(mz))
            seqs_harpe = bdd.get_sequences_from_mz(mz)
            bestt = (0, 0, -1)
            for seq_m in seqs_mascot:
                bestt = (len(seq_m), 0, -1)
                for seq_h in seqs_harpe:
                    bestt = best_tuple(bestt, best_match(seq_h, seq_m))
            reconized_mascot += bestt[0]
            if bestt[2] >= self.error_tol: #if harpe has not reconized 
                #at least error_tol aas don't take the result
                #~ print(bestt)
                reconized_harpe += bestt[1]
                good_reconized_harpe += bestt[2]
                if bestt[1] == bestt[2]:
                    seq_without_mistakes += 1
                else:
                    seq_with_mistakes += 1
                    
        return (reconized_mascot, reconized_harpe, good_reconized_harpe,
        seq_without_mistakes, seq_with_mistakes, nb_seqmascot)
    
    
    def best_seq_harpe(self, seq_list, prot_seq):
        """method to get the best sequence from a list of sequence and
        with a proteine sequence"""
        mascotseq = ''.join(prot_seq)
        best_seq = ""
        score_seq = -1
        res = ""
        bestt = (0, 0, -1)
        for (seq, score) in seq_list:
            tupleres = best_match(seq, mascotseq)
            bestt = best_tuple(bestt, tupleres)
            if bestt == tupleres:
                best_seq = seq
                score_seq = score 
        if len(best_seq) >= self.error_tol:
            res = best_match_error(best_seq, mascotseq)
        return res, score_seq
            
    def eval_all_seq_list(self, bdd):
        """method to create a csv file of all sequences """
        prot_seq = self.change_leucine(self.mascotprotres.get_sequence())
        listofmz = self.harperes.get_all_mz()
        listofintensity = self.harperes.get_all_intensity()
        listof_mz_int = zip(listofmz, listofintensity)
        reslist = []
        for mz, intensity in listof_mz_int:
            tab = {}
            tab["mz"] = mz
            tab["intensity"] = intensity
            #~ scoreh = self.harperes.get_score_from_mz(mz)
            #~ if len(scoreh) > 0:
                #~ tab["score_harpe"] = scoreh[0]
            scorem = self.mascotres.get_score_from_mz(mz)
            if len(scorem) > 0:
                tab["score_mascot"] = scorem[0]
            else:
                tab["score_mascot"] = ""
            seqs_mascot = self.change_leucine(\
            self.mascotres.get_sequences_from_mz(mz))
            if len(seqs_mascot) > 0:
                tab["seq_mascot"] = seqs_mascot[0]
            else:
                tab["seq_mascot"] = ""
            seqs_harpe = bdd.get_sequences_score_from_mz(mz)
            seq_harpe, scoreh = self.best_seq_harpe(seqs_harpe, prot_seq)
            if len(seq_harpe) > 0:
                tab["seq_harpe"] =  seq_harpe
                tab["score_harpe"] = scoreh
            else:
                tab["seq_harpe"] =  ""
                tab["score_harpe"] = ""
            reslist.append(tab)
        return reslist
            
        

if __name__ == '__main__':
    try:
        if len(sys.argv) == 3:
            if sys.argv[1] == "doc":
                docsv = True
            else:
                docsv = False
            if sys.argv[2] == "saga":
                doall = False
                docsv = False
                TEST = HarpeEvaluator("files/harpeSAGA.txt", 
                "files/mascotSAGA.txt", "files/mascotprot.txt")
            else:
                doall = True
                TEST = HarpeEvaluator("files/harpe.txt", 
                "files/mascot.txt", "files/mascotprot.txt")
    except:
        print("""Usage : %s "doc"|"*" "saga"|"*" """)
        exit(1)
    
    BDD = BddHarpe("bsanoelle4")
    if docsv:
        BDD.get_all_seq_mz()
        lres = TEST.eval_all_seq_list(BDD)
        print_all_seq_list(lres)
    else:
        print("Evaluation Top1 :\n")
        print_stat(TEST.eval_all_top1())
        if doall == True:
            BDD.get_all_seq_mz()
            print("\nEvaluation all Levels :\n")
            print_stat(TEST.eval_all(BDD))
