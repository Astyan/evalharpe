#!/usr/bin/env python
# encoding: utf-8
"""Tests for evaluate.py with bdd with unittest"""

import unittest
import numpy.testing as nt
import os

import evaluate
import bdd


class TestEvaluator(unittest.TestCase):
    """Class to test evaluate"""
    
    def setUp(self):
        """To do before each tests"""
        self.hevaluator = evaluate.HarpeEvaluator(os.path.join(".", 
        "filestest", "harpe.txt"), os.path.join(".", 
        "filestest", "mascot.txt"), os.path.join(".", 
        "filestest", "mascotprot.txt"))
        self.bdd = bdd.BddHarpe("bsanoelle4")
        
    def tearDown(self):
        """Clear after each tests"""
        pass
    
    def test_00_get_id_analyse(self):
        """ test get_id_analyse """
        self.bdd.get_id_analyse()
        self.assertEqual(self.bdd.id_analyse, 63)
    
    def test_01_get_id_peptide(self):
        """ test get_id_peptide """
        self.bdd.get_id_analyse()
        self.bdd.get_id_peptide()
        self.assertEqual(self.bdd.peptid_list[0], (631811L, '748.7783'))
    
    def test_02_get_sequences_from_mz(self):
        """ test get_sequences_from_mz"""
        self.bdd.get_all_seq_mz()
        self.assertEqual(self.bdd.get_sequences_from_mz('862.9161')[0],
        'MPCTEDYISII')
    
    def test_02_get_sequences_score_from_mz(self):
        """ test get_sequences_score_from_mz"""
        self.bdd.get_all_seq_mz()
        self.assertEqual(\
        self.bdd.get_sequences_score_from_mz('862.9161')[0], 
        ('MPCTEDYISII', 20.99657632654604))
        
        
        
if __name__ == '__main__':
    unittest.main()
