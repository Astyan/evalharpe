#!/usr/bin/env python
# encoding: utf-8
"""parse.py contain the classes to parse Mascot results
 and Harpe result"""
from __future__ import print_function
#~ import os


class MascotProtSeqParser(object):
    """Class to parse the proteine sequence find by mascot """
    
    def __init__(self, filepath):
        self.result_file = open(filepath, "r")
        self.result_parse = []
    
    def parse_file(self):
        """ A simple way to parse result file """
        for line in self.result_file:
            res = line.split()
            self.result_parse.append(res)
            
    def print_parser(self):
        """ print every line of the file (once parsed)"""
        for line in self.result_parse:
            print(line)
    
    def get_sequence(self):
        """ get the sequence """
        seq = ""
        for line in self.result_parse:
            for pep in line:
                seq = seq + pep
        return seq
    

class CoreParser(object):
    """Parent class that define how to parse the file and how to get MZ
    and sequences"""
    
    def __init__(self, filepath):
        self.result_file = open(filepath, "r")
        self.result_parse = []

    def parse_file(self):
        """ A simple way to parse result file """
        for line in self.result_file:
            res = line.split()
            self.result_parse.append(res)
            
    def print_parser(self):
        """ print every line of the file (once parsed)"""
        for line in self.result_parse:
            print(line)
            
    def get_all_something(self, col=0):
        """ generic way to get a list of all something from the parsed 
        file """
        listsomething = []
        for line in self.result_parse:
            listsomething.append(line[col])
        return listsomething
        
    def get_mz(self, i, col=0):
        """ generic way to get the MZ from the line i"""
        return self.result_parse[i][col]
    
    def get_sequence(self, i, col=0):
        """ generic way to get a sequence from the line i """
        return self.result_parse[i][col]
        
    def get_sequences_from_mz(self, mz, colmz=0, colseq=0):
        """ generic way to get all sequence matching to one MZ """
        listofsequences = []
        for line in self.result_parse:
            if line[colmz] == mz:
                listofsequences.append(line[colseq])
        return listofsequences

class MascotParser(CoreParser):
    """Parser for Mascot results """
    
    def __init__(self, filepath):
        CoreParser.__init__(self, filepath)
    
    def get_all_mz(self, col=3):
        """ get a list of all MZ in the parsed Mascot result File """
        return  CoreParser.get_all_something(self, col)
    
    def get_mz(self, i, col=3):
        """ get the MZ from the line i """
        return  CoreParser.get_mz(self, i, col) 
    
    def get_sequence(self, i, col=8):
        """ get the sequence from the line i """
        res = CoreParser.get_sequence(self, i, col)
        return res.split(".")[1]
    
    def get_sequences_from_mz(self, mz, colmz=3, colseq=8):
        """ get all sequence matching to one MZ """
        listofsequences = CoreParser.get_sequences_from_mz(self, mz, 
        colmz, colseq)
        listres = []
        for sequence in listofsequences:
            listres.append(sequence.split(".")[1])
        return listres
    
    def get_score_from_mz(self, mz, colmz=3, colseq=-1):
        "get the scores matching to one MZ """ 
        listofscore = CoreParser.get_sequences_from_mz(self, mz, 
        colmz, colseq)
        listres = []
        for score in listofscore:
            listres.append(score[:-1])
        return listres
        
    
class HarpeParser(CoreParser):
    """Parser for top one Harpe results """
    def __init__(self, filepath):
        CoreParser.__init__(self, filepath)
    
    def get_all_mz(self, col=5):
        """ get a list of all MZ in the parsed Harpe result File """
        return  CoreParser.get_all_something(self, col)
    
    def get_all_intensity(self, col=6):
        """get a list of all intensity in the parsed Harpe result File """
        return  CoreParser.get_all_something(self, col)
    
    def get_mz(self, i, col=5):
        """ get the MZ from the line i """
        return  CoreParser.get_mz(self, i, col) 
    
    def get_sequence(self, i, col=1):
        """ get the sequence from the line i """
        res = CoreParser.get_sequence(self, i, col)
        if res == "None": #added None to the website because some 
            #~ results have missing part
            return None
        else:
            return res[::-1] # invertion of the sequence because the 
            #~ sequence order of harpe is different of the mascot one 

    def get_sequences_from_mz(self, mz, colmz=5, colseq=1):
        """ get all sequence matching to one MZ """
        listofsequences = CoreParser.get_sequences_from_mz(self, mz, 
        colmz, colseq)
        listres = []
        for sequence in listofsequences:
            if sequence != "None": #added None to the website because 
                #~ some results have missing part
                listres.append(sequence[::-1]) #invertion of the 
                #~ sequence because the sequence order of harpe is 
                #~ different of the mascot one
                listres.append(sequence)
        return listres
    
    def get_score_from_mz(self, mz, colmz=5, colseq=2):
        "get the scores matching to one MZ """ 
        listofscore = CoreParser.get_sequences_from_mz(self, mz, 
        colmz, colseq)
        listres = []
        for score in listofscore:
            if score != "-1":
                listres.append(score)
        return listres

if __name__ == '__main__':
    TEST = HarpeParser("files/harpeSAGA.txt")
    TEST.parse_file()
    print(TEST.get_all_mz())
        
    
